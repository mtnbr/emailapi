<?php

use Illuminate\Support\Facades\Log;
use App\Emails;

Route::get( 'transmission', 'MainController@transmission' );

//Sample DB printed on screen
Route::get( 'db', function () {

    $emails = Emails::get();

    $a = pg_connect( "host=localhost port=5432 dbname=emailapi user=postgres password=postpost" );

    echo 'Postgre DB details:' . json_encode( pg_version( $a ) ) . '<br><br><br>';

    foreach ( $emails as $email ) {
        echo '<li>' . ' Track Id :' . $email->track_id . ' || Email Provider :' . $email->email_service_provider_id
            . ' || Variables : ' . $email->content . ' || customer_id :' . $email->customer_id . ' || ' . $email->status . '</li>';
    }
} );
