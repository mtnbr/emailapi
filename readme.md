Email API for ITERATO

**Requirements**

PostgreSQL 10.3
Nginx or Apache
PHP >= 7.1.3
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension
XML PHP Extension
Ctype PHP Extension
JSON PHP Extension


**Server Setup** 

composer install

/home/{username}/emailapi .env file add 

SPARKPOST_API_KEY={api key}
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_USERNAME={db_username}
DB_DATABASE={db_name}
DB_PASSWORD={db_password}
QUEUE_DRIVER=database

php artisan migrate for creating database tables

Command For Checking Email Status (crontab -e)
* * * * * php /home/{username}/emailapi/artisan schedule:run >> /dev/null 2>&1

Command Listen for Queues Sending Emails

sudo apt-get install supervisor
/etc/supervisor/conf.d
[program:laravel-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /home/{username}/emailapi/artisan queue:listen --tries=3
autostart=true
autorestart=true
user={username}
numprocs=8
redirect_stderr=true
stdout_logfile=/home/{username}/emailapi/worker.log