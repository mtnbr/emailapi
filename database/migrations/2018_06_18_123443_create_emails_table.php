<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {

        Schema::create( 'emails', function ( Blueprint $table ) {

            $table->increments( 'id' );

            $table->string( 'track_id' )->nullable()->index();

            $table->tinyInteger( 'email_service_provider_id' );

            $table->longText( 'content' ); //templateVars

            $table->integer( 'customer_id' )->unsigned();

            $table->tinyInteger( 'status' )->nullable(); // Mutator

            $table->foreign( 'customer_id' )->references( 'id' )->on( 'customers' )->onUpdate( 'cascade' )->onDelete( 'cascade' );

            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists( 'emails' );
    }
}
