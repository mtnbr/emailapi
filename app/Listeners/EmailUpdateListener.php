<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\Events\EmailUpdate as EmailUpdate;

class EmailUpdateListener
{

    public function __construct ()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\EmailUpdate $event
     */
    public function handle ( EmailUpdate $event )
    {
        $status = $event->emails->getOriginal( 'status' );

        switch ( $status ) {
            case 0:
                Log::info( 'Failed' );
                break;
            case 1:
                Log::info( 'Seen' );
                break;
            case 2:
                Log::info( 'Send' );
                break;
            case null:
                break;
            default :
                echo "unknown";
        }

    }
}
