<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use App\Emails;
use Illuminate\Support\Facades\Log;

class SendToExternalProvider implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emailCreated;
    protected $email;

    /**
     * Create a new job instance.
     *
     * @param $emailCreated
     * @param $email
     */
    public function __construct ( $emailCreated, $email )
    {
        $this->email = $email;
        $this->emailCreated = $emailCreated;
    }

    /**
     * Sends the data to external email service provider.
     * @return void
     */
    public function handle ()
    {

        $httpClient = new GuzzleAdapter( new Client() );

        $sparky = new SparkPost( $httpClient, [ 'key' => env( 'SPARKPOST_API_KEY' ) ] );

        //$variables = $this->emailCreated->content;

        $variables = [ 'name' => 'Metin', 'surname' => 'Barış' ];

        $promise = $sparky->transmissions->post( [
            'content'           => [ 'template_id' => 'my-first-email' ],
            'substitution_data' => $variables,
            'recipients'        => [
                [ 'address' => [ 'email' => $this->email ]
                ],
            ]
        ] );

        try {

            $response = $promise->wait();

            if ( $response->getStatusCode() == 200 ) {

                $responseBody = $response->getBody();

                $transmissionId = $responseBody[ 'results' ][ 'id' ];

                Emails::whereId( $this->emailCreated->id )->update( [ 'track_id' => $transmissionId, 'status' => 2 ] );

            }
        } catch ( \Exception $e ) {

            Log::warning( $e->getMessage() );

            Emails::whereId( $this->emailCreated->id )->update( [ 'status' => 0 ] );
        }
    }
}