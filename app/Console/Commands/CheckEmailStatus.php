<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Emails;
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class CheckEmailStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check The Status of Emails From External Provider';

    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle ()
    {
        $httpClient = new GuzzleAdapter( new Client() );
        $sparky = new SparkPost( $httpClient, [ "key" => env( 'SPARKPOST_API_KEY' ) ] );

        $transmissionIds = Emails::where( 'status', '2' )->pluck( 'track_id' )->toArray();

        $promise = $sparky->request( 'GET', 'message-events', [ 'transmission_ids' => $transmissionIds ] );

        try {

            $response = $promise->wait();

            $response_body = $response->getBody();

            foreach ( $response_body[ 'results' ] as $result ) {
                if ( $result[ 'type' ] === 'open' ) {
                    Emails::where( 'track_id', $result[ 'transmission_id' ] )->update( [ 'status' => 1 ] );
                }
            }
        } catch ( \Exception $e ) {
            echo $e->getCode() . "\n";
            //echo $e->getMessage() . "\n";
            // We can send a notification warning to internal system
        }
    }
}
