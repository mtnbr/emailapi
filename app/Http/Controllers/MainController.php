<?php

namespace App\Http\Controllers;

use App\Customers;
use App\Emails;
use App\Jobs\SendToExternalProvider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class MainController extends Controller
{
    /**
     * Main API request receiver from internal server, according to sender(email module) it passes data to other classes
     *
     * @param Request $request
     *
     * @return Request $request;
     */
    public function moduleSelect ( Request $request )
    {
        /*$request->validate( [
            'templateCode' => 'required|in:c1,c2,c3,c4',
            'templateVars' => 'required',
            'email'   => 'required|email'
          ] );
        */

        $email = $request->get( 'email' );
        $templateCode = $request->get( 'templateCode' );
        $templateVars = json_encode( $request->get( 'templateVars' ) );

        switch ( $templateCode ) {
            case 'c1':
                echo "c1moduleclasswillwork";
                $this->CreateEmail( 1, $email, $templateVars );
                break;
            case 'c2':
                echo "c2moduleclasswillwork";
                $this->CreateEmail( 2, $email, $templateVars );
                break;
            case 'c3':
                echo "c3moduleclasswillwork";
                $this->CreateEmail( 3, $email, $templateVars );
                break;
            case 'c4':
                echo "c4moduleclasswillwork";
                $this->CreateEmail( 4, $email, $templateVars );
                break;
            default :
                echo "modulenotfound";
        }

        dd( $request->all() );

    }

    /** New row at emails table and customers table for given request and dispatches the external emailer job
     * @param $emailServiceProviderId
     * @param $email
     * @param $templateVars
     * @return SendToExternalProvider
     */
    public function CreateEmail ( $emailServiceProviderId, $email, $templateVars )
    {

        $customer = Customers::firstOrCreate( [ 'customer_email' => $email ] );

        $emailCreated = Emails::create( [
            'email_service_provider_id' => $emailServiceProviderId,
            'customer_id'               => $customer->id,
            'content'                   => $templateVars
        ] );

        SendToExternalProvider::dispatch( $emailCreated, $email );

    }
}