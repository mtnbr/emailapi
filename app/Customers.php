<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';

    protected $fillable = [ 'customer_email' ];

    public function emails ()
    {
        return $this->hasMany( 'App\Emails', 'customer_id', 'id' );
    }
}
