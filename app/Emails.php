<?php

namespace App;

use App\Events\EmailUpdate;
use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
    protected $table = 'emails';

    protected $fillable = [ 'track_id', 'email_service_provider_id', 'content', 'status', 'customer_id' ];

    protected $dispatchesEvents = [
        'updated' => EmailUpdate::class,
    ];

    public function getStatusAttribute ( $value )
    {
        switch ( $value ) {
            case 0:
                return '<strong style="color:#a00006"> Failed  &#33; </strong>';
                break;
            case 1:
                return '<strong style="color:#2ca02c"> Seen  &#10003; </strong>';
                break;
            case 2:
                return '<strong style="color:#0c5460"> Send </strong>';
                break;
            case null:
                return '<strong style="color:#8e8e8e"> Unknown </strong>';
                break;
            default :
                return "Unknown";
        }
    }


}